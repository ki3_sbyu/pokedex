import {Request} from 'express'

interface IRequest extends Request {
  session: any
}

export = IRequest

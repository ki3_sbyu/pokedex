import mongoose from 'mongoose'

const {Schema} = mongoose

const User = new Schema({
  _id: {
    type: Number,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  picture: {
    type: String
  },
  favorites: {
    type: [Object],
    default: []
  }
})

export = mongoose.model('User', User)

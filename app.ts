import express, { Express, Response } from 'express'
import mongoose from 'mongoose'
import cookieParser from 'cookie-parser'
import session from 'cookie-session'
import bodyParser from 'body-parser'
import config from './config/config.json'

const CONNECTION_URI = process.env.MONGODB_URI || config.databaseURI
const PORT = process.env.PORT || 1337
const app: Express = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cookieParser())
app.use(session({ secret: config.secret.session}))
app.use(express.static('./front/build'))

mongoose.connect(CONNECTION_URI, { useNewUrlParser: true, useFindAndModify: false })

app.use('/', require('./routes/InfoRouter'))

app.listen(PORT)

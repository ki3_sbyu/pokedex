import IRequest from '../interfaces/IRequest'
import Path from 'path'
import {Response} from 'express'

export = async (req: IRequest, res: Response) => {
  res.sendFile(Path.resolve(__dirname, '../front/build/index.html'))
}

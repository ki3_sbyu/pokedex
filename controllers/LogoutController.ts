import IRequest from '../interfaces/IRequest'
import {Response} from 'express'

export = async (req: IRequest, res: Response) => {
  req.session.token = null
  res.end('{}')
}

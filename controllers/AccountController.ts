import IRequest from '../interfaces/IRequest'
import { Response } from 'express'
import User from '../models/UserModel'

export = async (req: IRequest, res: Response) => {
  try {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000')
    const token = req.session.token
    const body = req.body
    let response = null
    if (token) {
      const result = await User.findById(token)
      if (result) {
        response = result
        req.session.token = response._id
      } else {
        req.session = null
      }
    } else if (body._id) {
      const user = await User.findById(body._id)
      if (user) {
        response = user
      } else {
        const newUser = new User(body)
        const result = await newUser.save()
        response = result
      }
      req.session.token = response._id
    } else {
      req.session = null
    }
    res.end(JSON.stringify(response))
  } catch (err) {
    console.error(err)
    res.sendStatus(500).send(err)
  }
}

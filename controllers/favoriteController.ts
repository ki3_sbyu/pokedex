import IRequest from '../interfaces/IRequest'
import { Response } from 'express'
import User from '../models/UserModel'

export = async (req: IRequest, res: Response) => {
  const user: any = await User.findById(req.session.token)
  if (user && req.body.name && req.body.url) {
    let favorites = user.favorites
    const nameArr = favorites.map(x => x.name)

    if (~nameArr.indexOf(req.body.name)) {
      favorites = favorites.filter(x => x.name !== req.body.name)
    } else {
      favorites.push({ name: req.body.name, url: req.body.url })
    }

    await User.findByIdAndUpdate(req.session.token, { favorites: favorites })
    res.sendStatus(200)
  }
  else if (user) {
    res.setHeader('Content-Type', 'application/json')
    res.end(JSON.stringify(user.favorites))
  } else {
    return res.sendStatus(404)
  }
}

import { Router } from 'express'
import account from '../controllers/AccountController'
import index from '../controllers/IndexController'
import logout from '../controllers/LogoutController'
import favorite from '../controllers/FavoriteController'

const router = Router()

router.post('/favorites', favorite)
router.get('/logout', logout)
router.post('/account', account)
router.get('/*', index)

export = router
